#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

if "DJANGO_SETTINGS_MODULE" not in os.environ:
    os.environ["DJANGO_SETTINGS_MODULE"] = "base.settings"

import django

django.setup()
import logging
import xml.etree.ElementTree as ET
from modules.settings.models import SiteSettings
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned, ValidationError
from modules.patients.models import Patient

log = logging.getLogger('listen')


class Listen:

    def __init__(self):
        """
           Okunacak XML ve Okunduktan Sonra Tanşınıcak Dosya Yolları Site Ayarları Modelinden Çekiliyor..
       """
        self.console_log_enabled = False
        self.name = ""
        try:
            self.new_xml_path = SiteSettings.objects.get(Param='newXMLPath').Value
        except:
            print('newXMLPath Not Found')
        try:
            self.old_xml_path = SiteSettings.objects.get(Param='oldXMLPath').Value
        except:
            print('oldXMLPath Not Found')

    @staticmethod
    def has_change(remote_data, local_data, keys):
        change = False

        for key in keys:
            if not getattr(local_data, key) == remote_data[key]:
                change = True

                try:
                    local_value = getattr(local_data, key).id
                    remote_value = remote_data[key].id
                except:
                    local_value = getattr(local_data, key)
                    remote_value = remote_data[key]
                print(key, "anahtarı yerel değeri olan ", local_value,
                      ", uzak değer ", remote_value, " olarak değişmiş.")
        return change

    def get_files(self):
        """
           XML dosyalarını Çeken Fonksiyon
        """
        last_error = []
        for filename in os.listdir(self.new_xml_path):
            log.error('Founded File Name: ' + filename)
            if not filename.endswith('.xml'):
                continue
            fullname = os.path.join(self.new_xml_path, filename)
            tree = ET.parse(fullname)
            root = tree.getroot()
            if len(root.findall('patient')) > 0:
                for item in root.findall('patient'):
                    try:
                        data = {
                            'referance_code': item.find('ReferansNo').text,
                            'policlinic_code': item.find('PoliclinicCode').text,
                            'doctor_registration_code': item.find('DoctorRegistrationCode').text,
                            'doctor_name': item.find('Doctor').text,
                            'patient_firstname': item.find('FirstName').text,
                            'patient_lastname': item.find('LastName').text,
                            'patient_birth_date': datetime.strptime(item.find('BirthDate').text, "%d-%m-%Y"),
                            'patient_gender': int(item.find('Gender').text),
                            'patient_identification_number': item.find('IdentificationNumber').text,
                            'phone_number': item.find('PhoneNumber').text,
                            'visit_date': datetime.strptime(item.find('VisitDate').text, "%d-%m-%Y %H:%M"),
                            'next_visit_date': datetime.strptime(item.find('NextVisitDate').text, "%d-%m-%Y %H:%M"),
                            'doctor_note': item.find('DoctorNote').text,
                            'sending':False
                        }
                        try:
                            local_data = Patient.objects.get(referance_code=data['referance_code'])
                            if self.console_log_enabled:
                                print("{} kodlu nesne var.".format(data['referance_code']))

                            log.error("{} kodlu nesne var. Geçiliyor.".format(data['referance_code']))

                        except ObjectDoesNotExist:
                            if self.console_log_enabled:
                                print(
                                    "{} kodlu nesne bulunamadı. Yeni oluşturuluyor.".format(data['referance_code']))
                            local_data = Patient(**data)
                            local_data.full_clean()  # Hata sırasında ValidationError exception rise olur.
                            local_data.save()

                        except MultipleObjectsReturned:
                            raise ValidationError(
                                '{} kodlu birden fazla nesne döndü.'.format(data['referance_code']))

                    except ValidationError as ve:
                        verror = ''
                        for key, value in ve.message_dict.items():
                            verror = " ".join((key, "".join([x for x in value])))
                        log.error(verror)
                        if self.console_log_enabled:
                            print(verror)

                    except Exception as e:
                        if self.console_log_enabled:
                            print("GENEL HATA: ", e)
                        log.error("Beklenmedik Hata: {bh}\r\n{item}".format(bh=e, item=item))
                # Okunan Dosya Okunanlar Kalsörüne Taşınıyor.
                shutil.move("{}/{}".format(self.new_xml_path, filename), "{}/{}".format(self.old_xml_path, filename))
            else:
                if self.console_log_enabled:
                    print('Hasta Kaydı Bulunamadı.')
                log.error('{} adlı dosyada Hasta Kaydı Bulunamadı. Geçiliyor.'.format(filename))


def yardim():
    """
    Konsoldan çalıştırıldığında argüman yoksa ekrana basılacak yardım metni
    """
    print('''
    >listen.py get_files
    ornekler:
    printler icin;
    > listen.py get_files -p
    Dosya Adı ile Tek Dosya Güncellemesi için
    > listen.py -name dosyaAdı.xml
    ''')


if __name__ == "__main__":
    param = ''
    deger = ''
    this_proc = os.getpid()

    if len(sys.argv) < 2:
        yardim()
        sys.exit()

    a = Listen()
    islem = sys.argv[1]

    if len(sys.argv) > 2:
        try:
            param = sys.argv[2]
            try:
                deger = sys.argv[3]
            except:
                pass
        except:
            pass

        if param == "-name":
            a.name = deger
        if param == "-p":
            a.console_log_enabled = True

    if hasattr(a, islem):
        getattr(a, islem)()
    else:
        print('Bilinmeyen islev: %s' % islem)
