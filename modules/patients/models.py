from django.db import models

PATIENT_GENDERS = (
    (1, 'Kadın'),
    (2, 'Erkek'),
    (3, 'Belirtilmemiş'),
)


class Patient(models.Model):
    referance_code = models.IntegerField(unique=True, verbose_name="Hasta Referans Numarası")
    policlinic_code = models.CharField(max_length=4, verbose_name="Poliklinik Kodu")
    doctor_registration_code = models.CharField(max_length=8, verbose_name="Doktor Sicil Kodu")
    doctor_name = models.CharField(max_length=80, verbose_name="Doktorun Adı Soyadı")
    patient_firstname = models.CharField(max_length=55, verbose_name="Hasta Adı")
    patient_lastname = models.CharField(max_length=55, verbose_name="Hasta Soyadı")
    patient_birth_date = models.DateField(verbose_name="Doğum Tarihi")
    patient_gender = models.SmallIntegerField(choices=PATIENT_GENDERS, verbose_name="Cinsiyet", default=3)
    patient_identification_number = models.CharField(max_length=11, verbose_name="Kimlik Numarası")
    phone_number = models.CharField(max_length=10, verbose_name="Telefon Numarası")
    visit_date = models.DateTimeField(verbose_name="Ziyaret Tarihi")
    next_visit_date = models.DateTimeField(verbose_name="Bir Sonraki Ziyaret Tarihi", null=True, blank=True)
    doctor_note = models.TextField(verbose_name="Doktor Notu", null=True, blank=True, max_length=1000)
    sending = models.BooleanField(verbose_name="Gönderim Durumu", default=False)

    def __str__(self):
        return '{} {} - {}'.format(self.patient_firstname, self.patient_lastname, self.referance_code)

    class Meta:
        verbose_name = 'Hasta'
        verbose_name_plural = 'Hastalar'
