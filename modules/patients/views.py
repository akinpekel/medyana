import json
import logging
import time
from datetime import datetime

from asgiref.sync import sync_to_async
from django.contrib.auth import authenticate, login as do_login
from django.core import serializers
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from modules.libs.globalfunctions import get_vars, get_user, json_response, permit_response
from modules.patients.models import Patient

log = logging.getLogger(__name__)


@csrf_exempt
def login(request, **kwargs):
    post = get_vars(request)
    ResponseStatus = True
    Message = ''
    JsonContent = None
    try:
        username = post.get('username').replace(' ', '').replace('(', '').replace(')', '')
    except:
        username = post.get('username')

    RemoteUser = authenticate(username=username, password=post.get('password'))

    if RemoteUser is None:
        Message = "Kullanıcı adı veya şifre hatalı."
        ResponseStatus = False
    else:
        if RemoteUser.is_active:
            RemoteUser.backend = 'django.contrib.auth.backends.ModelBackend'
            do_login(request, RemoteUser)

            JsonContent = {
                'id': str(RemoteUser.id),
                'token': str(request.session.session_key),
                'user_is_staff': RemoteUser.is_staff,
                'user_is_superuser': RemoteUser.is_superuser,
                'email': RemoteUser.email,
                'name': RemoteUser.get_full_name(),
                'first_name': RemoteUser.first_name,
                'last_name': RemoteUser.last_name,
                'username': RemoteUser.username
            }

        else:
            Message = "Hesabınız Aktif Değil"
            ResponseStatus = False

    JsonResponse = {'status': ResponseStatus, 'message': Message, 'data': JsonContent}
    response = HttpResponse(json.dumps(JsonResponse, cls=serializers.json.DjangoJSONEncoder))
    return permit_response(response)


@csrf_exempt
def list_patients(request):
    request_post = get_vars(request)
    response_status = False
    response_data = {}
    response_message = ''
    user = get_user(request_post.get('token'))

    page = request_post.get('page')
    amount = request_post.get('amount')

    referance_code = request_post.get('referance_code')
    patient_name = request_post.get('patient_name')
    policlinic_code = request_post.get('policlinic_code')
    doctor_name = request_post.get('doctor_name')
    visit_date = request_post.get('visit_date')
    doctor_registration_code = request_post.get('doctor_registration_code')


    if not request_post.get('page'):
        page = 1

    if not request_post.get('amount'):
        amount = 10
    if user:
        try:
            q_filter = Q()
            if referance_code:
                q_filter &= Q(referance_code__icontains=referance_code)
            if patient_name:
                q_filter &= Q(
                    Q(patient_firstname__icontains=patient_name) | Q(patient_lastname__icontains=patient_name))
            if policlinic_code:
                q_filter &= Q(policlinic_code__icontains=policlinic_code)
            if doctor_name:
                q_filter &= Q(doctor_name__icontains=doctor_name)
            if visit_date:
                q_filter &= Q(visit_date=datetime.strptime(visit_date, '%Y-%m-%dT%H:%M'))
            if doctor_registration_code:
                q_filter &= Q(doctor_registration_code__icontains=doctor_registration_code)
            patientsObj = Patient.objects.filter(q_filter).order_by('-id')
            p = Paginator(patientsObj, amount)
            page = p.page(int(page))
            patients = page.object_list
            if patients.count() > 0:
                patient_data = []
                for patient in patients:
                    patient_data.append({
                        'id': patient.id,
                        'referance_code': patient.referance_code,
                        'policlinic_code': patient.policlinic_code,
                        'doctor_registration_code': patient.doctor_registration_code,
                        'doctor_name': patient.doctor_name,
                        'patient_firstname': patient.patient_firstname,
                        'patient_lastname': patient.patient_lastname,
                        'patient_gender': patient.get_patient_gender_display(),
                        'patient_identification_number': patient.patient_identification_number,
                        'phone_number': patient.phone_number,
                        'visit_date': patient.visit_date,
                        'next_visit_date': patient.next_visit_date,
                        'doctor_note': patient.doctor_note,
                    })
                response_status = True
                response_data['patients'] = patient_data
                response_data['page'] = {'page_count': p.num_pages, 'total': p.count}
            else:
                response_message = "Hasta Kaydı Bulunamadı."
        except Exception as e:
            response_message = 'Hasta Kaydı Bulunamadı.'
    else:
        response_message = 'Lütfen Giriş Yapınız.'
    return json_response(response_status, response_message, response_data)


@csrf_exempt
def get_detail_patients(request):
    request_post = get_vars(request)
    response_status = False
    response_data = {}
    response_message = ''
    user = get_user(request_post.get('token'))
    id = request_post.get('id')
    if user:
        try:
            patient = Patient.objects.get(id=id)
            response_data = {
                'id': patient.id,
                'referance_code': patient.referance_code,
                'policlinic_code': patient.policlinic_code,
                'doctor_registration_code': patient.doctor_registration_code,
                'doctor_name': patient.doctor_name,
                'patient_firstname': patient.patient_firstname,
                'patient_lastname': patient.patient_lastname,
                'patient_gender': patient.patient_gender,
                'patient_identification_number': patient.patient_identification_number,
                'phone_number': patient.phone_number,
                'visit_date': datetime.strftime(patient.visit_date, "%Y-%m-%dT%H:%M"),
                'next_visit_date': datetime.strftime(patient.next_visit_date, "%Y-%m-%dT%H:%M"),
                'doctor_note': patient.doctor_note,
                'patient_birth_date': patient.patient_birth_date,
            }
            response_status = True
        except Exception as e:
            response_message = 'Hasta Kaydı Bulunamadı.'
    else:
        response_message = 'Lütfen Giriş Yapınız.'
    return json_response(response_status, response_message, response_data)


@csrf_exempt
def update_patients(request):
    request_post = get_vars(request)
    response_status = False
    response_data = {}
    response_message = ''
    user = get_user(request_post.get('token'))
    id = request_post.get('id')
    if user:
        try:
            patient = Patient.objects.get(id=id)
            patient.referance_code = request_post.get('referance_code')
            patient.policlinic_code = request_post.get('policlinic_code')
            patient.doctor_registration_code = request_post.get('doctor_registration_code')
            patient.doctor_name = request_post.get('doctor_name')
            patient.patient_firstname = request_post.get('patient_firstname')
            patient.patient_lastname = request_post.get('patient_lastname')
            patient.patient_identification_number = request_post.get('patient_identification_number')
            patient.phone_number = request_post.get('phone_number')
            patient.visit_date = datetime.strptime(request_post.get('visit_date'), '%Y-%m-%dT%H:%M')
            patient.next_visit_date = datetime.strptime(request_post.get('next_visit_date'), '%Y-%m-%dT%H:%M')
            patient.doctor_note = request_post.get('doctor_note')
            patient.patient_birth_date = request_post.get('patient_birth_date')
            patient.patient_gender = request_post.get('patient_gender')
            patient.save()
            response_status = True
        except Exception as e:
            response_message = 'Hasta Kaydı Bulunamadı.'
    else:
        response_message = 'Giriş Yapınız'
    return json_response(response_status, response_message, response_data)


async def websocket_view(socket):
    await socket.accept()
    await socket.send_text('{"status":"connected"}')
    patients = await sync_to_async(Patient.objects.filter)(sending=False)
    while True:
        patients_json = await sync_to_async(serializers.serialize)('json', patients)
        await socket.send_text(patients_json)
        await sync_to_async(patients.update)(sending=True)
        time.sleep(10)
