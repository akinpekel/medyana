from django.contrib import admin

from modules.patients.models import Patient

admin.site.register(Patient)
