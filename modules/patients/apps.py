from django.apps import AppConfig


class PatientsConfig(AppConfig):
    name = 'modules.patients'
    verbose_name = 'Hasta Kayıtları'
