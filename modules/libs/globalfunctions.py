# -*- coding: utf8 -*-
import json

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse


def permit_response(response):
    response['Access-Control-Allow-Origin'] = '*'
    response['X-Frame-Options'] = '*'
    response['Access-Control-Allow-Credentials'] = 'true'
    response['Access-Control-Allow-Headers'] = 'Content-Type'
    response['content_type'] = 'application/json'
    return response


def json_response(status=False, message="", data={}):
    JsonData = {'status': status, 'message': message, 'data': data}
    JsonData = json.dumps(JsonData, cls=DjangoJSONEncoder)
    return permit_response(HttpResponse(JsonData))


def get_vars(request):
    if settings.DEBUG:
        if request.POST:
            post = request.POST
        else:
            post = request.GET
    else:
        post = request.POST

    return post


def get_user(token):
    try:
        s = Session.objects.get(session_key=token)
        user_id = s.get_decoded().get('_auth_user_id')
        auth_user = User.objects.get(pk=user_id)
        return auth_user
    except:
        return False
