from django.apps import AppConfig


class SettingsConfig(AppConfig):
    name = 'modules.settings'
    verbose_name = 'Ayarlar'
