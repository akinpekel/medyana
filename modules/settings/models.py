from django.db import models


class SiteSettings(models.Model):
    Name = models.CharField(max_length=155, verbose_name="Ayar Tanımı")
    Param = models.CharField(max_length=155, verbose_name="Parametre", unique=True)
    Value = models.TextField(verbose_name="Değer")

    class Meta:
        verbose_name_plural = 'Site Ayarları'
        verbose_name = 'Site Ayarı'

    def __str__(self):
        return self.Name