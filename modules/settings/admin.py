from django.contrib import admin

from modules.settings.models import SiteSettings

admin.site.register(SiteSettings)
