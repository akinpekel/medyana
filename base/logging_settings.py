# coding: utf-8
import os

custom_log_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'logs/')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(process)d %(thread)d %(module)s %(filename)s %(funcName)s %(lineno)s %(pathname)s %(name)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        # 'special': {
        #     '()': 'project.logging.SpecialFilter',
        #     'foo': 'bar',
        # },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'django': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(custom_log_path, 'django.log'),
            'formatter': 'verbose'
        },
        'modules': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(custom_log_path, 'medyana.log'),
            'formatter': 'verbose'
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'listen': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(custom_log_path, 'listen.log'),
            'formatter': 'verbose'
        },
        # 'mail_admins': {
        #     'level': 'DEBUG',
        #     'class': 'django.utils.log.AdminEmailHandler',
        #     'formatter': 'verbose'
        #     # 'filters': ['special']
        # }
    },
    'loggers': {
        'listen': {
            'handlers': ['listen'],
        },
        'django': {
            'handlers': ['django'],
            'level': 'INFO',
            'propagate': True,
        },
        'modules': {
            'handlers': ['modules'],
            'level': 'DEBUG',
            'propogate': True
        },
        'middlewres': {
            'handlers': ['modules'],
            'level': 'DEBUG',
            'propogate': True
        },
    }
}
